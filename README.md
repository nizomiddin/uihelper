<p align="center">
<strong> 👋 php dasturchilar uchun foydali funksiyalar. 👋 </strong> 
</p>

<p align="center">
    <a href="https://packagist.org/packages/adldap2/adldap2-laravel"><img src="https://img.shields.io/packagist/v/adldap2/adldap2-laravel.svg?style=flat-square"></a>
    <a href="https://packagist.org/packages/adldap2/adldap2-laravel"><img src="https://img.shields.io/packagist/dt/adldap2/adldap2-laravel.svg?style=flat-square"></a>
    <a href="https://packagist.org/packages/adldap2/adldap2-laravel"><img src="https://img.shields.io/packagist/l/adldap2/adldap2-laravel.svg?style=flat-square"></a>
</p>


<p align="center">
    <strong><a href="https://gitlab.com/nizomiddin/uihelper">Qo'llanma</a></strong>
</p>
<hr>

- **numberFormat()** Raqamni chiroyli ko'rinishda formatlash. 123456.1234000 = 123 456.1234
    * Kiruvchi parametrlar
        - @param int $number Formatlanishi kerak bo`lgan(Kiritilgan son-"KS") son. Odatda 0
        - @param int $decimal Formatlangan sonning butun qismi. Odatda 2 xona
        - @param string $decPoint Sonning butun va kasr qismini ajratib turuvchi belgi. Odatda '.'(nuqta)
        - @param string $thousandsSep Minglikni ajratuvchi belgi. Odatda ' '(bo`sh joy)
        - @param bool $removeZero 0(Nol)larni o`chirish. Odatda true
        - @param bool $showZero 0(Nol)larni ko`rsatish. Odatda true
          <br> Masalan:
        - `UIHelper::numberFormat(123456.0098000); // Natija: 123 456.01`
        - `UIHelper::numberFormat(123456.0098000,3,'.',' ',false); // Natija: 123 456.010`
        - `UIHelper::numberFormat(123456.0098000,3,'.',' ',true,false) Natija: 123 456.01`
    * Chiquvchi parametr:
        - @return string|null. Natija string qaytadi.
    *
      ```
      <?php 
        use Nizomiddin\Helper\UIHelper; 
        echo "<pre>";
        print_r( UIHelper::numberFormat(123456.0098000) ); 
        echo "</pre>";
      ?>
      ``` 
    *
      ```
      <?php 
        use Nizomiddin\Helper\UIHelper; 
        echo "<pre>";
        print_r( UIHelper::numberFormat(123456.0098000,3,'.',' ',true,false) );  
        echo "</pre>";
      ?>
      ```
  <hr> 
- **arrayToHtmlStringRecursive()** Array(massiv) ni string ga o'zgartirish
    * Kiruvchi parametrlar
        - @param array $arrayVar string ga aylantirilishi kerak bo`lgan array(massiv)
        - @param string $separator arrayning index(key)i bilan ajratib turuvchi string odatda ' '(bo`sh joy)
          <br> Masalan:
        - `UIHelper::arrayToHtmlStringRecursive($array,' ');`
        - `UIHelper::arrayToHtmlStringRecursive($model->errors,' > ');`
    * Chiquvchi parametr:
        - @return string|null Natija string qaytadi.

    *
       ```
       <?php 
         use Nizomiddin\Helper\UIHelper; 
         $array = [
                   'key01'=>'key11',
                   'key02'=>[
                     'key12'=>'key-111',
                     'key22'=>'key-222',
                   ],
                   'key03'=>'key13',
                 ];
         echo "<pre>";
         print_r( UIHelper::arrayToHtmlStringRecursive($array,' ') ); 
         print_r( UIHelper::arrayToHtmlStringRecursive($array,', ') ); 
         print_r( UIHelper::arrayToHtmlStringRecursive($array,'<hr>') ); 
         echo "</pre>";
       ?>
       ```

<h2 align="center"> ☺ ☺ ☺ </h2> 
